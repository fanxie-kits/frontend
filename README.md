## Fanxie Lab's Front-End Starter kit

Install dependencies:

```
npm install
```

Build for development and watch for changes:

```
npm run watch
```

Build for production:

```
npm run prod
```

For more information about building assets, see [https://laravel-mix.com/](https://laravel-mix.com/).
