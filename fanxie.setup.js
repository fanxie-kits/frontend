let fs = require('fs');

const vendors = {
    css: [
        {
            name: "Leaf CSS, by Fanxie Lab",
            package_name: "@fanxie/leaf-css",
            main_file_path: "/leaf.scss"
        },
        {
            name: "Bulma",
            package_name: "bulma",
            main_file_path: "/bulma.scss"
        },
        {
            name: "None",
            package_name: null
        }
    ]
};

module.exports = ( { chalk, inquirer, shell, package } ) => {

    return new Promise((resolve, reject) => {
        inquirer.prompt([
            {
                type: "list",
                name: "css_vendor",
                message: "What CSS library do you want to use?",
                choices: vendors.css.map(item => {
                    return {name: item.name, value: item}
                })
            }
        ]).then(answers => {
            let install_command = "npm install";

            if ( answers.css_vendor !== null ) {
                install_command += ` && npm install --save ${answers.css_vendor.package_name}`;
            }

            console.log(
                chalk.hex('#2affab')(`\nInstalling dependencies, this may take a few minutes...`)
            );

            shell.exec(install_command);

            console.log(
                chalk.hex('#2affab')(`\nDependencies installed.`)
            );

            const vendorsFile = '/src/scss/03-Generic/vendors.scss';
            fs.appendFile(`${shell.pwd()}${vendorsFile}`, `@import "~${answers.css_vendor.package_name}${answers.css_vendor.main_file_path}";\n`, err => {
                if ( err ) {
                    console.log(
                        chalk.hex('#f4d44d')(`\nCould not update file "${vendorsFile}"`)
                    );
                } else {
                    console.log(
                        chalk.hex('#2affab')(`\n    ${vendorsFile} updated.`)
                    );
                }

                resolve([
                    chalk.hex('#2affab')(`\n    All done. Use the following commands to start working on your project:`),
                    chalk.green(`\n\n       npm run dev or npm run watch\n\n`)
                ]);
            });
        });
    });
};
